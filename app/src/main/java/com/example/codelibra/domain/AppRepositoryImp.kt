package com.example.codelibra.domain

import android.content.Context
import com.example.codelibra.data.MyPref
import com.example.codelibra.data.local.dao.BookDao
import com.example.codelibra.data.local.dao.RatedDao
import com.example.codelibra.data.local.model.BookEntity
import com.example.codelibra.data.local.model.DownloadedBookData
import com.example.codelibra.data.local.model.RatedBookEntity
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.data.remote.model.BookRequest
import com.example.codelibra.data.remote.model.MapData
import com.example.codelibra.data.remote.model.UserData
import com.example.codelibra.data.remote.model.UserRequest
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flowOn
import java.io.File
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepositoryImp @Inject constructor(
    private val fireStore: FirebaseFirestore,
    private val auth: FirebaseAuth,
    private val storage: FirebaseStorage,
    private val bookDao: BookDao,
    private val ratedDao: RatedDao,
    private val pref: MyPref
) : AppRepository {
    private var map = HashMap<String, ArrayList<BookData>>()
    private var list = ArrayList<BookData>()
    private val ls = arrayListOf<UserData>()

    override fun getPage(id: String): Int =
        pref.getPage(id)

    override fun setPage(id: String, page: Int) =
        pref.setPage(id, page)

    override fun searchBook(search: String): List<BookData> {
        val ls = ArrayList<BookData>()
        list.forEach { item ->
            if (item.name.lowercase(Locale.getDefault())
                    .startsWith(search.lowercase(Locale.getDefault()))
            )
                ls.add(item)
        }
        return ls
    }

    override fun clearDownloadedData(context: Context) {
        clearAppCache(context)
        bookDao.deleteAll()
        ratedDao.deleteAllRated()
    }


    override fun allDownloadedBooks(): Flow<List<BookEntity>> = callbackFlow {
        val list = bookDao.getAllBooks()
        ls.clear()
        fireStore.collection("users")
            .whereEqualTo("userUID", getToken())
            .addSnapshotListener { value, _ ->
                value?.forEach{
                    ls.add(UserData(
                        firstName = it.data["firstName"].toString(),
                        lastName = it.data["lastName"].toString(),
                        email = it.data["email"].toString(),
                    ))
                }
            }
        trySend(list)

        awaitClose()
    }.flowOn(Dispatchers.IO)

    override fun downloadBook(data: BookData): Flow<Result<String>> = callbackFlow <Result<String>>  {
        val book = File.createTempFile("books", ".pdf")

        storage.getReferenceFromUrl(storage.reference.toString() + data.path)
            .getFile(book)
            .addOnSuccessListener {
                bookDao.add(BookEntity(id = 0, bookUID = data.uid, name = data.name, img = data.img, path = book.absolutePath))
                trySend(Result.success(book.absolutePath))
            }
            .addOnFailureListener {
                trySend(Result.failure(it))
            }
            .addOnProgressListener {}
        awaitClose()
    }.flowOn(Dispatchers.IO)

    override fun bookIsDownloaded(uid: String): DownloadedBookData {
        val data = bookDao.get(uid)
        return DownloadedBookData(data.isNotEmpty(), if (data.isNotEmpty()) data[0].path else "")
    }

    override fun bookIsRated(uid: String): Boolean {
        val data = ratedDao.getRated(uid)
        return data.isNotEmpty()
    }

    override fun getAllBooks(): List<BookData> =
        list

    override fun setRatingOfBook(data: BookData): Flow<Result<BookData>> = callbackFlow<Result<BookData>> {
        ratedDao.add(RatedBookEntity(0, data.uid))
        fireStore.collection("books")
            .document(data.uid)
            .set(
                BookRequest(
                name = data.name,
                img = data.img,
                path = data.path,
                rating = data.rating,
                voice = data.voice,
                category = data.category,
                description = data.description
            )
            )
            .addOnSuccessListener {
                trySend(Result.success(data))
            }
            .addOnFailureListener {
                trySend(Result.failure(it))
            }
        awaitClose()
    }.flowOn(Dispatchers.IO)

    override fun booksByCategory(category: String) : ArrayList<BookData> =
        map[category]!!

    override fun allBooksByCategory(): Flow<Result<List<MapData>>> = callbackFlow<Result<List<MapData>>> {
        fireStore.collection("books")
            .addSnapshotListener { value, error ->
                if (value != null) {
                    val map = HashMap<String, ArrayList<BookData>>()
                    val booksList = ArrayList<BookData>()
                    val list = ArrayList<MapData>()

                    value.forEach { item ->
                        val category = item.data["category"].toString()

                        if (!map.containsKey(category))
                            map[category] = ArrayList()

                        val data = BookData(
                            uid = item.id,
                            name = item.data["name"].toString(),
                            category = category,
                            img = item.data["img"].toString(),
                            path = item.data["path"].toString(),
                            description = item.data["description"].toString(),
                            rating = item.data["rating"].toString(),
                            voice = item.data["voice"].toString()
                        )

                        booksList.add(data)
                        map[category]?.add(data)
                    }

                    this@AppRepositoryImp.map = map
                    this@AppRepositoryImp.list = booksList

                    map.keys.forEach { key ->
                        list.add(MapData(key, map[key]!!))
                    }

                    trySend(Result.success(list))
                } else {
                    if (error != null) {
                        trySend(Result.failure(error))
                    } else {
                        trySend(Result.failure(Exception("Unknown error!")))
                    }
                }
            }
        awaitClose()
    }.flowOn(Dispatchers.IO)

    override fun userInfo(): UserData = ls[0]

    override fun signIn(email: String, password: String) : Flow<Result<Unit>> = callbackFlow<Result<Unit>> {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    pref.setToken(auth.currentUser?.uid.toString())
                    trySend(Result.success(Unit))
                } else {
                    trySend(Result.failure(Exception("Invalid email or password. Please try again!")))
                }
            }
        awaitClose()
    }.flowOn(Dispatchers.IO)

    override fun signUp(data: UserData, password: String): Flow<Result<Unit>> = callbackFlow<Result<Unit>> {
        auth.createUserWithEmailAndPassword(data.email, password)
            .addOnCompleteListener {task ->
                if (task.isSuccessful) {
                    fireStore.collection("users")
                        .add(
                            UserRequest(
                                firstName = data.firstName,
                                lastName = data.lastName,
                                email = data.email,
                                userUID = auth.currentUser?.uid.toString()
                            )
                        )
                        .addOnSuccessListener {
                            pref.setToken(auth.currentUser?.uid.toString())
                            trySend(Result.success(Unit))
                        }
                        .addOnFailureListener {
                            trySend(Result.failure(it))
                        }
                } else {
                    trySend(Result.failure(Exception("Email is already taken! Please try another to continue.")))
                }
            }
        awaitClose()
    }.flowOn(Dispatchers.IO)

    override fun getToken(): String =
        pref.getToken()

    override fun setToken(token: String) =
        pref.setToken(token)

    private fun clearAppCache(context: Context) {
        try {
            val cacheDir = context.cacheDir
            if (cacheDir != null && cacheDir.isDirectory) {
                deleteDir(cacheDir)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun deleteDir(dir: File): Boolean {
        return if (dir.isDirectory) {
            dir.list()?.forEach {
                val success = deleteDir(File(dir, it))
                if (!success) {
                    return false
                }
            }
            dir.delete()
        } else if (dir.isFile) {
            dir.delete()
        } else {
            false
        }
    }

}