package com.example.codelibra.domain

import android.content.Context
import com.example.codelibra.data.local.dao.BookDao
import com.example.codelibra.data.local.model.BookEntity
import com.example.codelibra.data.local.model.DownloadedBookData
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.data.remote.model.MapData
import com.example.codelibra.data.remote.model.UserData
import kotlinx.coroutines.flow.Flow

interface AppRepository {

    //last page
    fun getPage(id: String) : Int
    fun setPage(id: String, page: Int)

    //search
    fun searchBook(search: String) : List<BookData>

    //clear db in log out
    fun clearDownloadedData(context: Context)

    //home screen downloaded books
    fun allDownloadedBooks() : Flow<List<BookEntity>>

    //download book
    fun downloadBook(data: BookData) : Flow<Result<String>>
    fun bookIsDownloaded(uid: String) : DownloadedBookData
    fun bookIsRated(uid: String) : Boolean

    //search
    fun getAllBooks() : List<BookData>

    //detail
    fun setRatingOfBook(data: BookData) : Flow<Result<BookData>>

    //books
    fun booksByCategory(category: String) : List<BookData>

    //library
    fun allBooksByCategory() : Flow<Result<List<MapData>>>

    //profile screen
    fun userInfo() : UserData

    //auth
    fun signIn(email: String, password: String) : Flow<Result<Unit>>
    fun signUp(data: UserData, password: String) : Flow<Result<Unit>>
    fun getToken() : String
    fun setToken(token: String)
}