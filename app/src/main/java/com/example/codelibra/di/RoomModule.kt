package com.example.codelibra.di

import android.content.Context
import androidx.room.Room
import com.example.codelibra.data.local.AppDB
import com.example.codelibra.data.local.dao.BookDao
import com.example.codelibra.data.local.dao.RatedDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @[Provides Singleton]
    fun provideAppDB(@ApplicationContext context: Context) : AppDB =
        Room.databaseBuilder(context, AppDB::class.java, "CodeLibra.db")
            .allowMainThreadQueries()
            .build()

    @[Provides Singleton]
    fun provideBookDao(db: AppDB) : BookDao = db.getBookDao()

    @[Provides Singleton]
    fun provideRatedDao(db: AppDB) : RatedDao = db.getRatedDao()

}