package com.example.codelibra.di

import com.example.codelibra.domain.AppRepository
import com.example.codelibra.domain.AppRepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Binds
    fun bindRepository(imp: AppRepositoryImp) : AppRepository
}