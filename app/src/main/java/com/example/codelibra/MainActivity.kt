package com.example.codelibra

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.example.codelibra.utils.NetworkStatus
import com.example.codelibra.utils.NetworkValidator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var networkValidator: NetworkValidator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        networkValidator.init(
            blockHasNetwork = { lifecycleScope.launch { NetworkStatus.hasNetwork.emit(true) } },
            blockLostNetwork = { lifecycleScope.launch { NetworkStatus.hasNetwork.emit(false) } }
        )

    }

}