package com.example.codelibra.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkValidator @Inject constructor(@ApplicationContext private val context: Context) {
    var hasNetwork: Boolean = false
    fun init(blockHasNetwork: () -> Unit, blockLostNetwork: () -> Unit) {
        var networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()

        val connectivityCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                hasNetwork = true
                blockHasNetwork()
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                hasNetwork = false
                blockLostNetwork()
            }
        }

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.requestNetwork(networkRequest, connectivityCallback)
    }
}