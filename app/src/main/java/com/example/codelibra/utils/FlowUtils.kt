package com.example.codelibra.utils

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.flowWithLifecycle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

fun <T> Flow<T>.launchLifecycle(lifecycleCoroutineScope: LifecycleCoroutineScope, block: (T) -> Unit) {
    onEach { block(it) }
        .launchIn(lifecycleCoroutineScope)
}

fun logger(mes: String) {
    Log.d("TTT", mes)
}