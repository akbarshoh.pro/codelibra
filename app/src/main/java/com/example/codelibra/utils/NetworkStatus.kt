package com.example.codelibra.utils

import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

object NetworkStatus {
    val hasNetwork = MutableStateFlow(true)
}