package com.example.codelibra.presentation.screen.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.domain.AppRepository
import com.example.codelibra.utils.logger
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModelImp @Inject constructor(
    private val repo: AppRepository
) : ViewModel(), SearchViewModel {
    override val allBooksListFlow = MutableStateFlow<List<BookData>>(listOf())

    override fun allBooks() {
        viewModelScope.launch {
            allBooksListFlow.emit(repo.getAllBooks())
        }
    }

    override fun searchBook(search: String) {
        viewModelScope.launch {
            allBooksListFlow.emit(repo.searchBook(search))
        }
    }

}