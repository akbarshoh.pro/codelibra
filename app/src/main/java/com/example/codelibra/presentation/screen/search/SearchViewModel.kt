package com.example.codelibra.presentation.screen.search

import com.example.codelibra.data.remote.model.BookData
import kotlinx.coroutines.flow.Flow

interface SearchViewModel {
    val allBooksListFlow: Flow<List<BookData>>

    fun allBooks()
    fun searchBook(search: String)
}