package com.example.codelibra.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.data.remote.model.MapData
import com.example.codelibra.databinding.ItemCategoryBinding

class AuthorAdapter(val context: Context) : ListAdapter<MapData, AuthorAdapter.VH>(Diff) {
    private var openAll: ((String) -> Unit)? = null
    private var openBookDetail: ((BookData) -> Unit)? = null
    private var time = System.currentTimeMillis()

    object Diff : DiffUtil.ItemCallback<MapData>() {
        override fun areItemsTheSame(oldItem: MapData, newItem: MapData): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: MapData, newItem: MapData): Boolean {
            return oldItem == newItem
        }

    }

    inner class VH (private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {


        init {
            binding.hammasi.setOnClickListener {
                if (System.currentTimeMillis() - time > 500) {
                    openAll?.invoke(getItem(adapterPosition).category)
                    time = System.currentTimeMillis()
                }
            }
        }

        fun bind(data: MapData) {
            val childAdapter = InnerAdapter()

            binding.categoryName.text = data.category
            binding.rvChild.adapter = childAdapter
            binding.rvChild.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

            val ls = if (data.list.size > 4) data.list.subList(0, 5) else data.list

            childAdapter.submitList(ls)
            childAdapter.setRootClickListener {
                if (System.currentTimeMillis() - time > 500) {
                    openBookDetail?.invoke(it)
                    time = System.currentTimeMillis()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) =
        holder.bind(getItem(position))

    fun openAllFun(block: (String) -> Unit) {
        openAll = block
    }

    fun openBookDetailFun(block: (BookData) -> Unit) {
       openBookDetail = block
    }
}

