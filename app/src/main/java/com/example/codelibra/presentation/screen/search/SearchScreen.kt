package com.example.codelibra.presentation.screen.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.codelibra.databinding.ScreenSearchBinding
import com.example.codelibra.presentation.adapter.BooksByCategoryAdapter
import com.example.codelibra.utils.launchLifecycle
import com.example.codelibra.utils.logger
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchScreen : Fragment() {
    private val viewModel: SearchViewModel by viewModels<SearchViewModelImp>()
    private val adapter: BooksByCategoryAdapter by lazy { BooksByCategoryAdapter() }
    private var _binding: ScreenSearchBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvSearch.adapter = adapter
        binding.rvSearch.layoutManager = GridLayoutManager(requireContext(), 2)

        viewModel.allBooksListFlow.launchLifecycle(lifecycleScope) {
            adapter.submitList(it)
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.searchBook(newText!!)
                return true
            }
        })

        binding.backToMain.setOnClickListener {
            findNavController().popBackStack()
        }

        adapter.setRootClickListener {
            findNavController().navigate(SearchScreenDirections.actionSearchScreenToDetailScreen(it))
        }

        viewModel.allBooks()

        binding.searchView.requestFocus()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.showSoftInput(binding.searchView, InputMethodManager.SHOW_IMPLICIT)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
