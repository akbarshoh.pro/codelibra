package com.example.codelibra.presentation.screen.auth.signUp

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.codelibra.data.remote.model.UserData
import com.example.codelibra.databinding.ScreenSignUpBinding
import com.example.codelibra.presentation.dialog.NoConnectDialog
import com.example.codelibra.utils.NetworkStatus
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import ru.ldralighieri.corbind.view.clicks
import ru.ldralighieri.corbind.widget.textChanges

@AndroidEntryPoint
class SignUpScreen : Fragment() {
    private val viewModel: SignUpViewModel by viewModels<SignUpViewModelImp>()
    private var _binding: ScreenSignUpBinding? = null
    private val binding get() = _binding!!
    private var time = System.currentTimeMillis()
    private val bottomSheet = NoConnectDialog()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenSignUpBinding.inflate(inflater, container, false)
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {

            viewModel.progressState.launchLifecycle(lifecycleScope) {
                if (it) {
                    signUp.visibility = View.GONE
                    frameLoading.visibility = View.VISIBLE
                    progress.show()
                } else {
                    signUp.visibility = View.VISIBLE
                    frameLoading.visibility = View.GONE
                    progress.hide()
                }
            }

            viewModel.successSignInFlow.launchLifecycle(lifecycleScope) {
                findNavController().navigate(SignUpScreenDirections.actionSignUpScreenToMainScreen())
            }

            viewModel.errorMessageFlow.launchLifecycle(lifecycleScope) {error ->
                errorText.text = error
                errorText.visibility = View.VISIBLE
            }

            combine(
                editFirstName.textChanges().map { it.length > 3 },
                editFirstName.textChanges().map { it.length > 3 },
                editEmail.textChanges().map { it.endsWith("@gmail.com") && it.length > 12 },
                editPassword.textChanges().map { it.length > 7 },

                transform = {firstName, lastName, email, password ->
                    firstName && lastName && email && password
                }
            ).launchLifecycle(lifecycleScope) {
                signUp.isEnabled = it
            }

            NetworkStatus.hasNetwork.launchLifecycle(lifecycleScope) {
                if (it) {
                    if (bottomSheet.isAdded) {
                        bottomSheet.dismiss()
                    }
                } else {
                    bottomSheet.show(parentFragmentManager, "")
                }
            }

            signUp.clicks().launchLifecycle(lifecycleScope) {
                val firstName = editFirstName.text.toString()
                val lastName = editLastName.text.toString()
                val email = editEmail.text.toString()
                val password = editPassword.text.toString()

                viewModel.signUp(UserData(firstName, lastName, email), password)
            }

            binding.progress.indeterminateTintList = ColorStateList.valueOf(Color.WHITE)

            toSignInPage.setOnClickListener {
                if (System.currentTimeMillis() - time > 500) {
                    findNavController().navigate(SignUpScreenDirections.actionSignUpScreenToSignInScreen())
                    time = System.currentTimeMillis()
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}