package com.example.codelibra.presentation.screen.pdf

import kotlinx.coroutines.flow.Flow

interface PdfViewModel {
    val currentPageFlow: Flow<Int>

    fun getPage(id: String)
    fun setPage(id: String, page: Int)
}