package com.example.codelibra.presentation.screen.main.pages.library

import com.example.codelibra.data.remote.model.CategoryData
import com.example.codelibra.data.remote.model.MapData
import kotlinx.coroutines.flow.Flow

interface LibraryViewModel {
    val categoryListFlow: Flow<List<MapData>>
    val progressState: Flow<Boolean>
}