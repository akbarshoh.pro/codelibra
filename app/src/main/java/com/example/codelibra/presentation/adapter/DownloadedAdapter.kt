package com.example.codelibra.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.codelibra.R
import com.example.codelibra.data.local.model.BookEntity
import com.example.codelibra.databinding.ItemDownloadedBinding


class DownloadedAdapter : ListAdapter<BookEntity, DownloadedAdapter.VH>(Diff) {
    private var rootClickListener : ((BookEntity) -> Unit)? = null
    private var time = System.currentTimeMillis()

    object Diff : DiffUtil.ItemCallback<BookEntity>() {
        override fun areItemsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean =
            oldItem == newItem

    }

    inner class VH(private val binding: ItemDownloadedBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(bookData: BookEntity) {
            val words = bookData.name.split(" ")
            val res = words.subList(0, words.indexOf("Notes") + 1)
            val str =  StringBuilder()
            res.forEach {
                str.append("$it ")
            }

            binding.itemTitle.text = str.trim()

            Glide.with(binding.root)
                .load(bookData.img)
                .placeholder(R.drawable.ic_default)
                .into(binding.imgHolder)
        }

        init {
            binding.root.setOnClickListener {
                if (System.currentTimeMillis() - time > 500) {
                    rootClickListener?.invoke(getItem(adapterPosition))
                    time = System.currentTimeMillis()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(ItemDownloadedBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) =
        holder.bind(getItem(position))

    fun setRootClickListener(block : (BookEntity) -> Unit) {
        rootClickListener = block
    }

}