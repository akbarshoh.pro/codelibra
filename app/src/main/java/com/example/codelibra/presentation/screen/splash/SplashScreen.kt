package com.example.codelibra.presentation.screen.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.codelibra.R
import com.example.codelibra.databinding.ScreenSplashBinding
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
@SuppressLint("CustomSplashScreen")
class SplashScreen : Fragment() {
    private val viewModel: SplashViewModel by viewModels<SplashViewModelImp>()
    private var _binding: ScreenSplashBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.userTokenFlow.launchLifecycle(lifecycleScope) {token ->
            lifecycleScope.launch {
                if (token == "") {
                    delay(1000)
                    findNavController().navigate(SplashScreenDirections.actionSplashScreenToSignInScreen())
                } else {
                    delay(1000)
                    findNavController().navigate(SplashScreenDirections.actionSplashScreenToMainScreen())
                }
            }
        }
    }
}