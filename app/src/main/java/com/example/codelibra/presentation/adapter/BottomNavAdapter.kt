package com.example.codelibra.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.codelibra.presentation.screen.main.pages.home.HomeScreen
import com.example.codelibra.presentation.screen.main.pages.library.LibraryScreen
import com.example.codelibra.presentation.screen.main.pages.profile.ProfileScreen

class BottomNavAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int = 4
    override fun createFragment(position: Int): Fragment = when(position) {
        0 -> HomeScreen()
        1 -> LibraryScreen()
        else -> ProfileScreen()
    }
}