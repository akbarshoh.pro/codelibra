package com.example.codelibra.presentation.screen.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.data.local.model.DownloadedBookData
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModelImp @Inject constructor(
    private val repo: AppRepository
) : ViewModel(), DetailViewModel {
    override val bookDataFlow = MutableSharedFlow<BookData>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val bookIsDownloadedFlow = MutableSharedFlow<DownloadedBookData>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val bookIsRatedFlow = MutableSharedFlow<Boolean>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val successDownloadFlow = MutableSharedFlow<String>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val progressState = MutableStateFlow(0)

    override fun setRatingOfBook(data: BookData) {
        repo.setRatingOfBook(data)
            .onEach {
                it.onSuccess { value ->
                    bookDataFlow.emit(value)
                }
            }
            .launchIn(viewModelScope)
    }

    override fun downloadBook(data: BookData) {
        viewModelScope.launch {
            progressState.emit(1)
            repo.downloadBook(data)
                .onEach {
                    it.onSuccess {
                        progressState.emit(2)
                        successDownloadFlow.emit(it)
                    }
                }
                .launchIn(viewModelScope)
        }
    }

    override fun bookIsDownloaded(uid: String) {
        viewModelScope.launch {
            bookIsDownloadedFlow.emit(repo.bookIsDownloaded(uid))
        }
    }

    override fun bookIsRated(uid: String) {
        viewModelScope.launch {
            bookIsRatedFlow.emit(repo.bookIsRated(uid))
        }
    }
}