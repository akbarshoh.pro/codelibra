package com.example.codelibra.presentation.screen.main.pages.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.codelibra.R
import com.example.codelibra.databinding.ScreenHomeBinding
import com.example.codelibra.presentation.adapter.DownloadedAdapter
import com.example.codelibra.presentation.screen.main.MainScreenDirections
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeScreen : Fragment(R.layout.screen_home) {
    private val viewModel: HomeViewModel by viewModels<HomeViewModelImp>()
    private val adapter: DownloadedAdapter by lazy { DownloadedAdapter() }
    private var _binding: ScreenHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvDownloads.adapter = adapter
        binding.rvDownloads.layoutManager = GridLayoutManager(requireContext(), 2)

        viewModel.downloadedBooks.launchLifecycle(lifecycleScope) {
            adapter.submitList(it)
        }

        adapter.setRootClickListener {
            findNavController().navigate(MainScreenDirections.actionMainScreenToPdfScreen(it.path, it.name))
        }

        viewModel.emptyPlaceHolderFlow.launchLifecycle(lifecycleScope) {
            binding.empty.isVisible = it
        }

        viewModel.allDownloadedBooks()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}