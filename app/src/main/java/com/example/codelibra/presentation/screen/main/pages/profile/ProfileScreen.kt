package com.example.codelibra.presentation.screen.main.pages.profile

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.codelibra.R
import com.example.codelibra.databinding.ScreenProfileBinding
import com.example.codelibra.presentation.screen.main.MainScreenDirections
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint
import ru.ldralighieri.corbind.view.clicks

@AndroidEntryPoint
class ProfileScreen : Fragment(R.layout.screen_profile) {
    private var _binding: ScreenProfileBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ProfileViewModel by viewModels<ProfileViewModelImp>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.userDataFlow.launchLifecycle(lifecycleScope) {
            binding.fullName.text = it.lastName + " " + it.firstName
            binding.email.text = it.email
        }

        viewModel.userInfo()

        binding.logOut.clicks().launchLifecycle(lifecycleScope) {
            openDialog()
        }

        binding.telegram.clicks().launchLifecycle(lifecycleScope) {
            openLink(requireContext(), "https://t.me/gitauz")
        }

        binding.insta.clicks().launchLifecycle(lifecycleScope) {
            openLink(requireContext(), "https://www.instagram.com/gita.uz?igsh=M2E3Z3M1eXgxbzhl")
        }

        binding.youtube.clicks().launchLifecycle(lifecycleScope) {
            openLink(requireContext(), "https://www.youtube.com/results?search_query=gita+dasturchilar+akademiyasi")
        }

        binding.facebook.clicks().launchLifecycle(lifecycleScope) {
            openLink(requireContext(), "https://www.facebook.com/gitauzofficial/")
        }

    }

    private fun openDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("Log Out")
            .setMessage("Are you sure you want to log out?")
            .setPositiveButton("Yes") { dialog, _ ->
                viewModel.setToken("")
                viewModel.clearDownloadedData(requireContext())
                findNavController().navigate(MainScreenDirections.actionMainScreenToSignInScreen())
                dialog.dismiss()
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun openLink(context: Context, url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }

}