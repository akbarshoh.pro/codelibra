package com.example.codelibra.presentation.screen.books

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.codelibra.R
import com.example.codelibra.databinding.ScreernBooksBinding
import com.example.codelibra.presentation.adapter.BooksByCategoryAdapter
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BooksScreen : Fragment(R.layout.screern_books) {
    private val navArgs: BooksScreenArgs by navArgs<BooksScreenArgs>()
    private val viewModel: BooksViewModel by viewModels<BooksViewModelImp>()
    private val adapter: BooksByCategoryAdapter by lazy { BooksByCategoryAdapter() }
    private var _binding: ScreernBooksBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreernBooksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.category.text = navArgs.category
        binding.rvBooks.adapter = adapter
        binding.rvBooks.layoutManager = GridLayoutManager(requireContext(), 2)

        adapter.setRootClickListener {
            findNavController().navigate(BooksScreenDirections.actionBooksScreenToDetailScreen(it))
        }

        viewModel.booksListFlow.launchLifecycle(lifecycleScope) {
            adapter.submitList(it)
        }

        binding.backToMain.setOnClickListener {
            findNavController().popBackStack()
        }

        viewModel.booksByCategory(navArgs.category)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}