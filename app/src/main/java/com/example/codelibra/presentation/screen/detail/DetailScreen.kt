package com.example.codelibra.presentation.screen.detail

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.databinding.DialogBottomSheetBinding
import com.example.codelibra.databinding.ScreenBookDetailBinding
import com.example.codelibra.presentation.dialog.NoConnectDialog
import com.example.codelibra.utils.NetworkStatus
import com.example.codelibra.utils.launchLifecycle
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailScreen : Fragment() {
    private val viewModel: DetailViewModel by viewModels<DetailViewModelImp>()
    private val navArgs: DetailScreenArgs by navArgs()
    private var _binding: ScreenBookDetailBinding? = null
    private val binding get() = _binding!!
    private var path = ""
    private val bottomSheet = NoConnectDialog()
    private var time = System.currentTimeMillis()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenBookDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            Glide.with(binding.root)
                .load(navArgs.data.img)
                .into(binding.imgCoverImage)

            val words = navArgs.data.name.split(" ")
            val res = words.subList(0, words.indexOf("Notes") + 1)
            val str =  StringBuilder()
            res.forEach {
                str.append("$it ")
            }

            binding.progress.indeterminateTintList = ColorStateList.valueOf(Color.WHITE)

            tvBookName.text = str.trim()
            tvAbout.text = navArgs.data.description
            ratingBar.rating = if (navArgs.data.voice.toInt() == 0) 0F else navArgs.data.rating.toFloat() / navArgs.data.voice.toFloat()
        }

        binding.rating.text = (navArgs.data.rating.toFloat() / navArgs.data.voice.toInt()).toString().substring(0, 3)

        viewModel.bookDataFlow.launchLifecycle(lifecycleScope) { data ->
            val averageRating = data.rating.toFloat() / data.voice.toInt()
            binding.ratingBar.rating = averageRating
            binding.rating.text = averageRating.toString().substring(0, 3)
            binding.rateBook.visibility = View.GONE
        }

        viewModel.progressState.launchLifecycle(lifecycleScope) {
            if (it == 1) {
                binding.downloadBook.visibility = View.GONE
                binding.frameLoading.visibility = View.VISIBLE
                binding.progress.show()
            } else if (it == 2) {
                binding.downloadBook.text = "Read now"
                binding.downloadBook.visibility = View.VISIBLE
                binding.frameLoading.visibility = View.GONE
                binding.progress.hide()
            }
        }

        viewModel.bookIsDownloadedFlow.launchLifecycle(lifecycleScope) {
            if (it.isDownload) {
                binding.downloadBook.text = "Read now"
                path = it.path
                binding.frameLoading.visibility = View.GONE
            }
        }

        viewModel.bookIsRatedFlow.launchLifecycle(lifecycleScope) {
            if (it) {
                binding.rateBook.visibility = View.GONE
            } else {
                binding.rateBook.visibility = View.VISIBLE
            }
        }

        viewModel.successDownloadFlow.launchLifecycle(lifecycleScope) {
            path = it
        }

        binding.downloadBook.setOnClickListener {
            NetworkStatus.hasNetwork.launchLifecycle(lifecycleScope) {
                if (it) {
                    if (bottomSheet.isAdded) {
                        bottomSheet.dismiss()
                    }
                    if (binding.downloadBook.text == "Download")
                        viewModel.downloadBook(navArgs.data)
                    else
                        findNavController().navigate(DetailScreenDirections.actionDetailScreenToPdfScreen(path, navArgs.data.name))
                } else {
                    if (!bottomSheet.isAdded) {
                        bottomSheet.show(childFragmentManager, "")
                    }
                }
            }
        }

        binding.rateBook.setOnClickListener {
            NetworkStatus.hasNetwork.launchLifecycle(lifecycleScope) {
                if (it) {
                    if (bottomSheet.isAdded) {
                        bottomSheet.dismiss()
                    }
                    if (System.currentTimeMillis() - time > 1000) {
                        showBottomSheetDialog()
                        time = System.currentTimeMillis()
                    }
                } else {
                    if (!bottomSheet.isAdded) {
                        bottomSheet.show(childFragmentManager, "")
                    }
                }
            }
        }

        viewModel.bookIsDownloaded(navArgs.data.uid)
        viewModel.bookIsRated(navArgs.data.uid)

        binding.backToMain.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun showBottomSheetDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        val dialog = DialogBottomSheetBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(dialog.root)

        val words = navArgs.data.name.split(" ")
        val res = words.subList(0, words.indexOf("Notes") + 1)
        val str =  StringBuilder()
        res.forEach {
            str.append("$it ")
        }

        dialog.appCompatTextView3.text = str.trim()

        dialog.btnSave.setOnClickListener {
            val currentRating = navArgs.data.rating.toFloat()
            val newRating = currentRating + dialog.rateBook.rating
            val voice = navArgs.data.voice.toInt() + 1

            val data = BookData(
                uid = navArgs.data.uid,
                name = navArgs.data.name,
                img = navArgs.data.img,
                path = navArgs.data.path,
                rating = newRating.toString(),
                voice = voice.toString(),
                category = navArgs.data.category,
                description = navArgs.data.description
            )

            viewModel.setRatingOfBook(data)

            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

