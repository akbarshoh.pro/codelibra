package com.example.codelibra.presentation.screen.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.example.codelibra.R
import com.example.codelibra.databinding.ScreenMainBinding
import com.example.codelibra.presentation.adapter.BottomNavAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainScreen : Fragment(R.layout.screen_main) {
    private var _binding: ScreenMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by viewModels<MainViewModelImp>()
    private lateinit var adapter: BottomNavAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenMainBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = BottomNavAdapter(childFragmentManager, lifecycle)

        binding.pager.adapter = adapter
        binding.pager.isUserInputEnabled = false

        binding.pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                binding.bottomNavigation.menu.getItem(position).isChecked = true
            }
        })

        binding.bottomNavigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> binding.pager.currentItem = 0
                R.id.library -> binding.pager.currentItem = 1
                else -> binding.pager.currentItem = 2
            }
            return@setOnItemSelectedListener true
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}