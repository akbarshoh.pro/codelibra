package com.example.codelibra.presentation.screen.pdf

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.codelibra.databinding.ScreenPdfReaderBinding
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint
import java.io.File

@AndroidEntryPoint
class PdfScreen : Fragment() {
    private val viewModel: PdfViewModel by viewModels<PdfViewModelImp>()
    private val navArgs: PdfScreenArgs by navArgs()
    private var _binding: ScreenPdfReaderBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenPdfReaderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val words = navArgs.name.split(" ")
        val res = words.subList(0, words.indexOf("Notes"))
        val str =  StringBuilder()
        res.forEach {
            str.append("$it ")
        }

        binding.pdfView.recycle()

        viewModel.currentPageFlow.launchLifecycle(lifecycleScope) {
            binding.pdfView
                .fromUri(Uri.fromFile(File(navArgs.path)))
                .defaultPage(it)
                .onPageChange { page, _ ->
                    viewModel.setPage(str.toString(), page)
                }
                .load()
        }

        viewModel.getPage(str.toString())

        binding.backToMain.setOnClickListener {
            findNavController().popBackStack()
        }

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(requireActivity(), object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {}
            }
            )

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}