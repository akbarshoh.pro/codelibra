package com.example.codelibra.presentation.screen.detail

import com.example.codelibra.data.local.model.DownloadedBookData
import com.example.codelibra.data.remote.model.BookData
import kotlinx.coroutines.flow.Flow

interface DetailViewModel  {
    val bookDataFlow: Flow<BookData>
    val bookIsDownloadedFlow: Flow<DownloadedBookData>
    val bookIsRatedFlow: Flow<Boolean>
    val successDownloadFlow: Flow<String>
    val progressState: Flow<Int>

    fun setRatingOfBook(data: BookData)
    fun downloadBook(data: BookData)
    fun bookIsDownloaded(uid: String)
    fun bookIsRated(uid: String)
}