package com.example.codelibra.presentation.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.codelibra.databinding.DialogNoconnectBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class NoConnectDialog : BottomSheetDialogFragment() {
    private lateinit var binding: DialogNoconnectBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogNoconnectBinding.inflate(inflater, container, false)
        this.dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return binding.root
    }
}