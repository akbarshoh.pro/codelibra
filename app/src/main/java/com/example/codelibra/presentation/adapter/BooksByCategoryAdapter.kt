package com.example.codelibra.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.codelibra.R
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.databinding.ItemBookByCategoryBinding

class BooksByCategoryAdapter : ListAdapter<BookData, BooksByCategoryAdapter.VH>(Diff) {
    private var rootClickListener : ((BookData) -> Unit)? = null
    private var time = System.currentTimeMillis()

    object Diff : DiffUtil.ItemCallback<BookData>() {
        override fun areItemsTheSame(oldItem: BookData, newItem: BookData): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: BookData, newItem: BookData): Boolean =
            oldItem == newItem

    }

    inner class VH(private val binding: ItemBookByCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(bookData: BookData) {
            binding.itemRating.text = if (bookData.voice.toInt() == 0) {
                "Rating: 0"
            } else {
                val averageRating = bookData.rating.toFloat() / bookData.voice.toInt()
                "Rating: ${averageRating.toString().substring(0, 3)}"
            }
            val words = bookData.name.split(" ")
            val res = words.subList(0, words.indexOf("Notes") + 1)
            val str =  StringBuilder()
            res.forEach {
                str.append("$it ")
            }
            binding.itemTitle.text = str.trim()
            Glide.with(binding.root)
                .load(bookData.img)
                .placeholder(R.drawable.ic_default)
                .into(binding.imgHolder)
        }

        init {
            binding.root.setOnClickListener {
                if (System.currentTimeMillis() - time > 500) {
                    rootClickListener?.invoke(getItem(adapterPosition))
                    time = System.currentTimeMillis()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(ItemBookByCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) =
        holder.bind(getItem(position))

    fun setRootClickListener(block : (BookData) -> Unit) {
        rootClickListener = block
    }

}