package com.example.codelibra.presentation.screen.splash

import kotlinx.coroutines.flow.Flow

interface SplashViewModel {
    val userTokenFlow: Flow<String>
}