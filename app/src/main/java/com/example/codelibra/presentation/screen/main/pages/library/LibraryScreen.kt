package com.example.codelibra.presentation.screen.main.pages.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codelibra.R
import com.example.codelibra.databinding.ScreenLibraryBinding
import com.example.codelibra.presentation.adapter.AuthorAdapter
import com.example.codelibra.presentation.screen.main.MainScreenDirections
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint
import ru.ldralighieri.corbind.view.clicks

@AndroidEntryPoint
class LibraryScreen : Fragment(R.layout.screen_library) {
    private val viewModel: LibraryViewModel by viewModels<LibraryViewModelImp>()
    private val adapter: AuthorAdapter by lazy { AuthorAdapter(requireContext()) }
    private var _binding: ScreenLibraryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenLibraryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvCategory.adapter = adapter
        binding.rvCategory.layoutManager = LinearLayoutManager(requireContext())

        adapter.openAllFun {
            findNavController().navigate(MainScreenDirections.actionMainScreenToBooksScreen(it))
        }

        viewModel.progressState.launchLifecycle(lifecycleScope) {
            if (it) {
                binding.progressBar.show()
                binding.rvCategory.visibility = View.GONE
            } else {
                binding.progressBar.hide()
                binding.rvCategory.visibility = View.VISIBLE
            }
        }

        adapter.openBookDetailFun {
            findNavController().navigate(MainScreenDirections.actionMainScreenToDetailScreen(it))
        }

        binding.toSearchScreen.clicks().launchLifecycle(lifecycleScope) {
            findNavController().navigate(MainScreenDirections.actionMainScreenToSearchScreen())
        }

        viewModel.categoryListFlow.launchLifecycle(lifecycleScope) {
            adapter.submitList(it)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}