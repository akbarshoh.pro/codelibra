package com.example.codelibra.presentation.screen.auth.signIn

import kotlinx.coroutines.flow.Flow

interface SignInViewModel {
    val successSignInFlow: Flow<Unit>
    val errorMessageFlow: Flow<String>
    val progressState: Flow<Boolean>

    fun signIn(email: String, password: String)
}