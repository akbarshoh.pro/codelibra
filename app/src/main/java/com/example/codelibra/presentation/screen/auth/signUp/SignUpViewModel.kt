package com.example.codelibra.presentation.screen.auth.signUp

import com.example.codelibra.data.remote.model.UserData
import kotlinx.coroutines.flow.Flow

interface SignUpViewModel {
    val successSignInFlow: Flow<Unit>
    val errorMessageFlow: Flow<String>
    val progressState: Flow<Boolean>

    fun signUp(data: UserData, password: String)
}