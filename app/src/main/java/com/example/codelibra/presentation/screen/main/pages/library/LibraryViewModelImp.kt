package com.example.codelibra.presentation.screen.main.pages.library

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.data.remote.model.CategoryData
import com.example.codelibra.data.remote.model.MapData
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LibraryViewModelImp @Inject constructor(
    private val repo: AppRepository
) : ViewModel(), LibraryViewModel {
    override val categoryListFlow = MutableSharedFlow<List<MapData>> (replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val progressState = MutableStateFlow(false)

    init {
        viewModelScope.launch {
            progressState.emit(true)
            repo.allBooksByCategory()
                .onEach {
                    it.onSuccess {
                        progressState.emit(false)
                        categoryListFlow.emit(it)
                    }
                    it.onFailure {
                        progressState.emit(false)
                    }
                }
                .launchIn(viewModelScope)
        }
    }

}