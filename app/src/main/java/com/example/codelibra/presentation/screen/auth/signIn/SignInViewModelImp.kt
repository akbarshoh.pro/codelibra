package com.example.codelibra.presentation.screen.auth.signIn

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModelImp @Inject constructor(
    private val repo: AppRepository
) : ViewModel(), SignInViewModel {
    override val successSignInFlow = MutableSharedFlow<Unit>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val errorMessageFlow = MutableSharedFlow<String>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val progressState = MutableStateFlow(false)

    override fun signIn(email: String, password: String) {
        viewModelScope.launch {
            progressState.emit(true)
            repo.signIn(email, password)
                .onEach {
                    it.onSuccess {
                        progressState.emit(false)
                        successSignInFlow.emit(Unit)
                    }
                    it.onFailure {
                        progressState.emit(false)
                        errorMessageFlow.emit(it.message.toString())
                    }
                }
                .launchIn(viewModelScope)
        }
    }

}