package com.example.codelibra.presentation.screen.main.pages.profile

import android.content.Context
import com.example.codelibra.data.remote.model.UserData
import kotlinx.coroutines.flow.Flow

interface ProfileViewModel {
    val userDataFlow: Flow<UserData>

    fun userInfo()
    fun setToken(token: String)
    fun clearDownloadedData(context: Context)
}