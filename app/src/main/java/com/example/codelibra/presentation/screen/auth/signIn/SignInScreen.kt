package com.example.codelibra.presentation.screen.auth.signIn

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.codelibra.databinding.ScreenSignInBinding
import com.example.codelibra.presentation.dialog.NoConnectDialog
import com.example.codelibra.utils.NetworkStatus
import com.example.codelibra.utils.launchLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.view.clicks
import ru.ldralighieri.corbind.widget.textChanges

@AndroidEntryPoint
class SignInScreen : Fragment() {
    private val viewModel: SignInViewModel by viewModels<SignInViewModelImp>()
    private var _binding: ScreenSignInBinding? = null
    private val binding get() = _binding!!
    private var time = System.currentTimeMillis()
    private val bottomSheet = NoConnectDialog()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenSignInBinding.inflate(inflater, container, false)
        return _binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progress.indeterminateTintList = ColorStateList.valueOf(Color.WHITE)

        binding.apply {

            viewModel.progressState.launchLifecycle(lifecycleScope) {
                if (it) {
                    signIn.visibility = View.GONE
                    frameLoading.visibility = View.VISIBLE
                    progress.show()
                } else {
                    signIn.visibility = View.VISIBLE
                    frameLoading.visibility = View.GONE
                    progress.hide()
                }
            }

            viewModel.successSignInFlow
                .onEach {
                    findNavController().navigate(SignInScreenDirections.actionSignInScreenToMainScreen())

                }
                .launchIn(lifecycleScope)

            viewModel.errorMessageFlow.launchLifecycle(lifecycleScope) {error ->
                errorText.text = error
                errorText.visibility = View.VISIBLE
            }

            combine(
                 editEmail.textChanges().map { it.endsWith("@gmail.com") && it.length > 12 },
                 editPassword.textChanges().map { it.length > 7 },

                transform = {email, password ->
                    email && password
                }
            ).launchLifecycle(lifecycleScope) {
                signIn.isEnabled = it
            }

            NetworkStatus.hasNetwork.launchLifecycle(lifecycleScope) {
                if (it) {
                    if (bottomSheet.isAdded) {
                        bottomSheet.dismiss()
                    }
                } else {
                    bottomSheet.show(parentFragmentManager, "")
                }
            }

            signIn.clicks().launchLifecycle(lifecycleScope) {
                val email = editEmail.text.toString()
                val password = editPassword.text.toString()
                viewModel.signIn(email, password)
            }

            toSignUpPage.setOnClickListener {
                if (System.currentTimeMillis() - time > 500) {
                    findNavController().navigate(SignInScreenDirections.actionSignInScreenToSignUpScreen())
                    time = System.currentTimeMillis()
                }
            }
        }

    }

}