package com.example.codelibra.presentation.screen.pdf

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PdfViewModelImp @Inject constructor(
    private val repo: AppRepository
) : ViewModel(), PdfViewModel {

    override val currentPageFlow = MutableSharedFlow<Int>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)

    override fun getPage(id: String) {
        viewModelScope.launch {
            currentPageFlow.emit(repo.getPage(id))
        }
    }

    override fun setPage(id: String, page: Int) =
        repo.setPage(id, page)
}