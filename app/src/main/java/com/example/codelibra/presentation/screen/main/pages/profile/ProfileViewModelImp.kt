package com.example.codelibra.presentation.screen.main.pages.profile

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.data.remote.model.UserData
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModelImp @Inject constructor(private val repo: AppRepository) : ViewModel(), ProfileViewModel {
    override val userDataFlow = MutableSharedFlow<UserData>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)

    override fun userInfo() {
        viewModelScope.launch {
            userDataFlow.emit(repo.userInfo())
        }
    }

    override fun setToken(token: String) =
        repo.setToken(token)

    override fun clearDownloadedData(context: Context) =
        repo.clearDownloadedData(context)

}