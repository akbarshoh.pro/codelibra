package com.example.codelibra.presentation.screen.main

import androidx.lifecycle.ViewModel
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModelImp @Inject constructor(
    repo: AppRepository
) : ViewModel(), MainViewModel {
}