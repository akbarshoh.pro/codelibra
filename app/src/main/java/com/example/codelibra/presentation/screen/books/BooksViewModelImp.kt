package com.example.codelibra.presentation.screen.books

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BooksViewModelImp @Inject constructor(
    private val repo: AppRepository
) : ViewModel(), BooksViewModel {
    override val booksListFlow = MutableSharedFlow<List<BookData>>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)

    override fun booksByCategory(category: String) {
        viewModelScope.launch {
            booksListFlow.emit(repo.booksByCategory(category))
        }
    }
}