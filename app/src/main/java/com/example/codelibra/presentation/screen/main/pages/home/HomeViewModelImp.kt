package com.example.codelibra.presentation.screen.main.pages.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codelibra.data.local.model.BookEntity
import com.example.codelibra.data.remote.model.BookData
import com.example.codelibra.domain.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModelImp @Inject constructor(
    private val repo: AppRepository
) : ViewModel(), HomeViewModel {
    override val downloadedBooks =
        MutableSharedFlow<List<BookEntity>>(replay = 1, onBufferOverflow = BufferOverflow.DROP_LATEST)
    override val emptyPlaceHolderFlow = MutableStateFlow(false)

    override fun allDownloadedBooks() {
        viewModelScope.launch {
            repo.allDownloadedBooks()
                .onEach {
                    if (it.isEmpty()) emptyPlaceHolderFlow.emit(true)
                    else {
                        downloadedBooks.emit(it)
                        emptyPlaceHolderFlow.emit(false)
                    }
                }
                .launchIn(viewModelScope)
        }
    }

}