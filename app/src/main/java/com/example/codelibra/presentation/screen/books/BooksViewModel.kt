package com.example.codelibra.presentation.screen.books

import com.example.codelibra.data.remote.model.BookData
import kotlinx.coroutines.flow.Flow

interface BooksViewModel {
    val booksListFlow: Flow<List<BookData>>

    fun booksByCategory(category: String)
}