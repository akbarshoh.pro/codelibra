package com.example.codelibra.presentation.screen.main.pages.home

import com.example.codelibra.data.local.model.BookEntity
import com.example.codelibra.data.remote.model.BookData
import kotlinx.coroutines.flow.Flow

interface HomeViewModel {
    val downloadedBooks: Flow<List<BookEntity>>
    val emptyPlaceHolderFlow: Flow<Boolean>

    fun allDownloadedBooks()
}