package com.example.codelibra.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.codelibra.data.local.dao.BookDao
import com.example.codelibra.data.local.dao.RatedDao
import com.example.codelibra.data.local.model.BookEntity
import com.example.codelibra.data.local.model.RatedBookEntity

@Database(entities = [BookEntity::class, RatedBookEntity::class], version = 1)
abstract class AppDB : RoomDatabase() {
    abstract fun getBookDao() : BookDao
    abstract fun getRatedDao() : RatedDao
}