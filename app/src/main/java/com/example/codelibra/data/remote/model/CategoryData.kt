package com.example.codelibra.data.remote.model

data class CategoryData (
    val category: String,
    val img: Int
)