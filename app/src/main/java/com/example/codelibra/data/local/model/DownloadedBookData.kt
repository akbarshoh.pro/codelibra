package com.example.codelibra.data.local.model

data class DownloadedBookData (
    val isDownload: Boolean,
    val path: String
)