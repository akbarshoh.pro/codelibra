package com.example.codelibra.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rated_books")
data class RatedBookEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val bookUID: String
)