package com.example.codelibra.data.remote.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BookData (
    val uid: String,
    val name: String,
    val img: String,
    val path: String,
    val rating: String,
    val voice: String,
    val category: String,
    val description: String
) : Parcelable