package com.example.codelibra.data

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MyPref @Inject constructor(private val pref: SharedPreferences) {

    fun getToken() : String =
        pref.getString("token", "").toString()

    fun setToken(token: String) =
        pref.edit().putString("token", token).apply()

    fun getPage(id: String) : Int =
        pref.getInt(id, 0)

    fun setPage(id: String, page: Int) =
        pref.edit().putInt(id, page).apply()

    fun isFirstEnter() : Boolean {
        return pref.getBoolean("FIRST", true)
    }

    fun introFinished() {
        pref.edit().putBoolean("FIRST", false).apply()
    }

}