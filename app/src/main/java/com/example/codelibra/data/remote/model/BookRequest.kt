package com.example.codelibra.data.remote.model

data class BookRequest (
    val name: String,
    val img: String,
    val path: String,
    val rating: String,
    val voice: String,
    val category: String,
    val description: String
)