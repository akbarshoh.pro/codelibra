package com.example.codelibra.data.remote.model

data class MapData(
    val category: String,
    val list: List<BookData>
)
