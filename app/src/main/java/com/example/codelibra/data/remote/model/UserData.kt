package com.example.codelibra.data.remote.model

data class UserData(
    val firstName: String,
    val lastName: String,
    val email: String
)
