package com.example.codelibra.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "download_books")
data class BookEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val bookUID: String,
    val name: String,
    val img: String,
    val path: String
)