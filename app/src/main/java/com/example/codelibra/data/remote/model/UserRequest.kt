package com.example.codelibra.data.remote.model

data class UserRequest(
    val firstName: String,
    val lastName: String,
    val email: String,
    val userUID: String
)
