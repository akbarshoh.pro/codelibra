package com.example.codelibra.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.codelibra.data.local.model.RatedBookEntity

@Dao
interface RatedDao {
    @Insert
    fun add(data: RatedBookEntity)
    @Query("DELETE from rated_books")
    fun deleteAllRated()

    @Query("SELECT * FROM rated_books WHERE bookUID = :bookUID LIMIT 1")
    fun getRated(bookUID: String) : List<RatedBookEntity>
}