package com.example.codelibra.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.codelibra.data.local.model.BookEntity

@Dao
interface BookDao {
    @Insert
    fun add(data: BookEntity)

    @Query("SELECT * FROM download_books WHERE bookUID = :bookUID LIMIT 1")
    fun get(bookUID: String) : List<BookEntity>

    @Query("SELECT * FROM download_books")
    fun getAllBooks() : List<BookEntity>

    @Query("DELETE from download_books")
    fun deleteAll()
}